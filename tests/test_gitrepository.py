# tests/test_gitrepository.py

from typer.testing import CliRunner

from pelosi import cli

runner = CliRunner()


def test_no_command():
    result = runner.invoke(
        cli.app,
        [
            "-A",
            "git-repository",
            "--url",
            "https://api.launchpad.net/devel/~ubuntu-kernel/ubuntu/+source/linux/+git/version-seeds",  # noqa: E501
        ],
    )
    assert result.exit_code == 0
    assert result.stdout.startswith("http")


def test_list_mp():
    result = runner.invoke(
        cli.app,
        [
            "-A",
            "git-repository",
            "--url",
            "https://api.launchpad.net/devel/~ubuntu-kernel/ubuntu/+source/linux/+git/bionic/",
            "list-mp",
        ],
    )
    assert result.exit_code == 0
    assert result.stdout.startswith("http")


def test_show():
    result = runner.invoke(
        cli.app,
        [
            "-A",
            "git-repository",
            "--url",
            "https://api.launchpad.net/devel/~ubuntu-kernel/ubuntu/+source/linux/+git/bionic/",
            "show",
            "--pretty=json",
        ],
    )
    assert result.exit_code == 0
    assert "self_link" in result.stdout
