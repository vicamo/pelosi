# tests/test_gitrepositories.py

from typer.testing import CliRunner

from pelosi import cli

runner = CliRunner()


def test_count_for_repack():
    result = runner.invoke(cli.app, ["-A", "git-repositories", "count-for-repack"])
    assert result.exit_code == 0
    assert int(result.stdout)


def test_get():
    result = runner.invoke(cli.app, ["-A", "git-repositories", "get", "ubuntu/+source/linux"])
    assert result.exit_code == 0
    assert result.stdout.startswith("http")
