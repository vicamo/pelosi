# tests/test_cli.py

from typer.testing import CliRunner

from pelosi import __app_name__, __version__, cli

runner = CliRunner()


def test_version():
    result = runner.invoke(cli.app, ["--version"])
    assert result.exit_code == 0
    assert f"{__app_name__} v{__version__}\n" in result.stdout


def test_anonymous_login():
    result = runner.invoke(cli.app, ["--anonymous"])
    assert result.exit_code == 0
    assert "" == result.stdout
