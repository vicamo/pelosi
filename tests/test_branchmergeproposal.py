# tests/test_branchmergeproposal.py

from typer.testing import CliRunner

from pelosi import cli

runner = CliRunner()


def test_no_command():
    result = runner.invoke(
        cli.app,
        [
            "-A",
            "branch-merge-proposal",
            "--url",
            "https://api.launchpad.net/devel/~xnox/ubuntu/+source/linux/+git/bionic/+merge/412576",
        ],
    )
    assert result.exit_code == 0
    assert result.stdout.startswith("http")


def test_show():
    result = runner.invoke(
        cli.app,
        [
            "-A",
            "branch-merge-proposal",
            "--url",
            "https://api.launchpad.net/devel/~xnox/ubuntu/+source/linux/+git/bionic/+merge/412576",
            "show",
            "--pretty=json",
        ],
    )
    assert result.exit_code == 0
    assert "self_link" in result.stdout
