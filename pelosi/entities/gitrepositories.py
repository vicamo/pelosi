"""Pelosi gitrepositories subcommand."""
# pelosi/entities/gitrepositories.py

from datetime import datetime

import typer
from typing import List, Optional

from pelosi.launchpad import (
    GitRepositoriesSortOrder,
    GitRepositoryInformationType,
    pretty_dumps,
)


app = typer.Typer()


@app.command("count-for-repack")
def count_for_repack(ctx: typer.Context):
    """Get number of repositories qualifying for a repack.

    This command wraps git_repositories.countRepositoriesForRepack."""
    lp = ctx.obj
    print(lp.git_repositories.countRepositoriesForRepack())


@app.command()
def get(
    ctx: typer.Context,
    path: str = typer.Argument(
        ...,
        help="Repository path.",
    ),
    pretty: Optional[str] = typer.Option(
        "self-link",
        "--pretty",
        "-p",
        help="Pretty-print the content. The option string might be either "
        "'self-link', 'json' or 'format', which prints the self_link feild "
        "of the passed resource object only, or in json dumps, or in "
        "an arbitrary format as specified by the string following a ':' "
        "character. For example, 'format:{self_link}' give the same result "
        "as 'self-link' does.",
    ),
):
    """Find a repository by its path.

    This command wraps git_repositories.getByPath."""
    lp = ctx.obj

    repo = lp.git_repositories.getByPath(path=path)
    print(pretty_dumps(repo, pretty))


@app.command("get-default")
def get_default(
    ctx: typer.Context,
    owner: Optional[str] = typer.Option(
        None,
        "--owner",
        "-o",
        help="The owner of this Git repository.",
    ),
    target: str = typer.Argument(
        ...,
        help="The target of the repository.",
    ),
    pretty: Optional[str] = typer.Option(
        "self-link",
        "--pretty",
        "-p",
        help="Pretty-print the content. The option string might be either "
        "'self-link', 'json' or 'format', which prints the self_link feild "
        "of the passed resource object only, or in json dumps, or in "
        "an arbitrary format as specified by the string following a ':' "
        "character. For example, 'format:{self_link}' give the same result "
        "as 'self-link' does.",
    ),
):
    """Get the default repository for a target.

    This command wraps git_repositories.getDefaultRepository and
    git_repositories.getDefaultRepositoryForOwner."""
    # lp = ctx.obj
    # if owner:
    #    repo = lp.git_repositories.getDefaultRepositoryForOwner(
    #        owner=owner, target=target
    #    )
    # else:
    #    repo = lp.git_repositories.getDefaultRepository(target=target)
    #
    # print(pretty_dumps(repo, pretty))
    raise NotImplementedError("FIXME")


@app.command("get-visibility-info")
def get_visibility_info(
    ctx: typer.Context,
    pretty: Optional[str] = typer.Option(
        "self-link",
        "--pretty",
        "-p",
        help="Pretty-print the content. The option string might be either "
        "'self-link', 'json' or 'format', which prints the self_link feild "
        "of the passed resource object only, or in json dumps, or in "
        "an arbitrary format as specified by the string following a ':' "
        "character. For example, 'format:{self_link}' give the same result "
        "as 'self-link' does.",
    ),
    person: str = typer.Argument(
        ...,
        help="The person whose repository visibility is being checked.",
    ),
    names: List[str] = typer.Argument(
        ...,
        help="List of repository unique names.",
    ),
):
    """Count the named repositories visible.

    Anonymous requesters don't get any information.

    Return a dict with the following values:
    person_name -- the displayname of the person.
    visible_repositories -- a list of the unique names of the repositories which
                            the requester and specified person can both see.

    This API call is provided for use by the client Javascript. It is not
    designed to efficiently scale to handle requests for large numbers of
    repositories.

    This command wraps git_repositories.getRepositoryVisibilityInfo."""
    # lp = ctx.obj
    #
    # result = lp.git_repositories.getRepositoryVisibilityInfo(
    #    person=person, repository_names=names
    # )
    # print(pretty_dumps(repo, pretty))
    raise NotImplementedError("FIXME")


@app.command()
def list(
    ctx: typer.Context,
    order_by: GitRepositoriesSortOrder = typer.Option(
        GitRepositoriesSortOrder.newest.value,
        "--order-by",
        "-o",
        case_sensitive=False,
        help="Sort order.",
    ),
    since: Optional[datetime] = typer.Option(
        None,
        "--since",
        "-s",
        help="Return only repositories whose date_last_modified is greater "
        "than or equal to this date..",
    ),
    target: Optional[str] = typer.Option(
        None,
        "--target",
        "-t",
        help="Link to a git_target.",
    ),
    pretty: Optional[str] = typer.Option(
        "self-link",
        "--pretty",
        "-p",
        help="Pretty-print the content. The option string might be either "
        "'self-link', 'json' or 'format', which prints the self_link feild "
        "of the passed resource object only, or in json dumps, or in "
        "an arbitrary format as specified by the string following a ':' "
        "character. For example, 'format:{self_link}' give the same result "
        "as 'self-link' does.",
    ),
):
    """List repositories.

    This lists repositores specified by --target, modified since date specified
    by --since, and sorted by --order-by.

    This wraps getRepositories."""
    lp = ctx.obj

    repos = lp.git_repositories.getRepositories(
        modified_since_date=since, order_by=order_by.value, target=target
    )

    for repo in repos:
        print(pretty_dumps(repo, pretty))


@app.command("list-for-repack")
def list_for_repack(
    ctx: typer.Context,
    limit: Optional[int] = typer.Option(
        0,
        "--limit",
        "-l",
    ),
    pretty: Optional[str] = typer.Option(
        "self-link",
        "--pretty",
        "-p",
        help="Pretty-print the content. The option string might be either "
        "'self-link', 'json' or 'format', which prints the self_link feild "
        "of the passed resource object only, or in json dumps, or in "
        "an arbitrary format as specified by the string following a ':' "
        "character. For example, 'format:{self_link}' give the same result "
        "as 'self-link' does.",
    ),
):
    """Get the top badly packed repositories.

    This command wraps getRepositoriesForRepack."""
    # lp = ctx.obj
    #
    # repos = lp.git_repositories.getRepositoriesForRepack(limit=limit)
    # for repo in repos:
    #    print(pretty_dumps(repo, pretty))
    raise NotImplementedError("FIXME")


@app.command()
def new(
    ctx: typer.Context,
    information_type: GitRepositoryInformationType = typer.Option(
        GitRepositoryInformationType.PUBLIC.value,
        "--information-type",
        "-t",
        case_sensitive=False,
        help="The type of information contained in this repository.",
    ),
    owner: Optional[str] = typer.Option(
        ...,
        "--owner",
        "-o",
        help="The owner of this Git repository. This controls who can modify the repository.",
    ),
    target: Optional[str] = typer.Option(
        ...,
        "--target",
        "-t",
        help="The target of the repository.",
    ),
    name: str = typer.Argument(
        ...,
        help="The repository name. Keep very short, unique, and descriptive, "
        "because it will be used in URLs.",
    ),
):
    """Create a Git repository.

    This command wraps git_repositories.new."""
    # lp = ctx.obj
    #
    # result = lp.git_repositories.new(
    #    information_type=information_type,
    #    owner=owner,
    #    target=target,
    #    name=name,
    # )
    raise NotImplementedError("FIXME")


@app.command("set-default")
def set_default(
    ctx: typer.Context,
    owner: Optional[str] = typer.Option(
        None,
        "--owner",
        "-o",
        help="The owner of this Git repository.",
    ),
    pretty: Optional[str] = typer.Option(
        "self-link",
        "--pretty",
        "-p",
        help="Pretty-print the content. The option string might be either "
        "'self-link', 'json' or 'format', which prints the self_link feild "
        "of the passed resource object only, or in json dumps, or in "
        "an arbitrary format as specified by the string following a ':' "
        "character. For example, 'format:{self_link}' give the same result "
        "as 'self-link' does.",
    ),
    target: str = typer.Argument(
        ...,
        help="The target of the repository.",
    ),
    repo: str = typer.Argument(
        ...,
        help="A git repository.",
    ),
):
    """Set the default repository for a target.

    This command wraps git_repositories.setDefaultRepository and
    git_repositories.setDefaultRepositoryForOwner."""
    # lp = ctx.obj
    #
    # if owner:
    #    lp.git_repositories.setDefaultRepositoryForOwner(
    #        owner=owner, repo=repo, target=target
    #    )
    # else:
    #    lp.git_repositories.setDefaultRepository(target=target, repo=repo)
    raise NotImplementedError("FIXME")


@app.callback()
def main() -> None:
    """Subcommand group for git_repositories."""
    return
