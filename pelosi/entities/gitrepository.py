"""Pelosi gitrepository subcommand."""
# pelosi/entities/gitrepository.py

import typer
from typing import List, Optional

from pelosi.launchpad import (
    BranchMergeProposalQueueStatus,
    pretty_dumps,
)


app = typer.Typer()


@app.command()
def show(
    ctx: typer.Context,
    pretty: Optional[str] = typer.Option(
        "self-link",
        "--pretty",
        "-p",
        help="Pretty-print the content. The option string might be either "
        "'self-link', 'json' or 'format', which prints the self_link feild "
        "of the passed resource object only, or in json dumps, or in "
        "an arbitrary format as specified by the string following a ':' "
        "character. For example, 'format:{self_link}' give the same result "
        "as 'self-link' does.",
    ),
):
    """Display content of this git repository."""
    repo = ctx.obj
    print(pretty_dumps(repo, pretty))


@app.command("list-mp")
def list_mp(
    ctx: typer.Context,
    # merged_revision_ids
    pretty: Optional[str] = typer.Option(
        "self-link",
        "--pretty",
        "-p",
        help="Pretty-print the content. The option string might be either "
        "'self-link', 'json' or 'format', which prints the self_link feild "
        "of the passed resource object only, or in json dumps, or in "
        "an arbitrary format as specified by the string following a ':' "
        "character. For example, 'format:{self_link}' give the same result "
        "as 'self-link' does.",
    ),
    statuses: Optional[List[BranchMergeProposalQueueStatus]] = typer.Option(
        None,
        "--statuses",
        "-s",
        case_sensitive=False,
        help="A list of merge proposal statuses to filter by.",
    ),
):
    """Return matching BranchMergeProposals.

    This command wraps getMergeProposals."""
    repo = ctx.obj

    mps = repo.getMergeProposals(status=statuses)
    for mp in mps:
        print(pretty_dumps(mp, pretty))


@app.callback(invoke_without_command=True)
def main(
    ctx: typer.Context,
    url: Optional[str] = typer.Option(..., "--url", "-u", help="Resource url."),
) -> None:
    """Subcommand group for git_repository."""
    lp = ctx.obj

    repo = lp.load(url)

    if not ctx.invoked_subcommand:
        print(repo)
        raise typer.Exit()

    ctx.obj = repo
