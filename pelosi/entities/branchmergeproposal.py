"""Pelosi branchmergeproposal subcommand."""
# pelosi/entities/branchmergeproposal.py

import typer
from typing import Optional

from pelosi.launchpad import pretty_dumps

app = typer.Typer()


@app.command("link-bug")
def link_bug(
    ctx: typer.Context,
    bug: int = typer.Argument(...),
):
    """Link a bug to this merge proposal.

    This command wraps branch_merge_proposal.linkBug."""
    # mp = ctx.obj
    # mp.linkBug(bug=bug)
    raise NotImplementedError("FIXME")


@app.command()
def show(
    ctx: typer.Context,
    pretty: Optional[str] = typer.Option(
        "self-link",
        "--pretty",
        "-p",
        help="Pretty-print the content. The option string might be either "
        "'self-link', 'json' or 'format', which prints the self_link feild "
        "of the passed resource object only, or in json dumps, or in "
        "an arbitrary format as specified by the string following a ':' "
        "character. For example, 'format:{self_link}' give the same result "
        "as 'self-link' does.",
    ),
):
    """Display content of this merge proposal."""
    print(pretty_dumps(ctx.obj, pretty))


@app.command("unlink-bug")
def unlink_bug(
    ctx: typer.Context,
    bug: int = typer.Argument(...),
):
    """Unlink a bug from this merge proposal.

    This command wraps branch_merge_proposal.unlinkBug."""
    # mp = ctx.obj
    # mp.unlinkBug(bug=bug)
    raise NotImplementedError("FIXME")


@app.callback(invoke_without_command=True)
def main(
    ctx: typer.Context,
    url: Optional[str] = typer.Option(..., "--url", "-u", help="Resource url."),
) -> None:
    """Subcommand group for branch_merge_proposal."""
    lp = ctx.obj
    mp = lp.load(url)

    if not ctx.invoked_subcommand:
        print(mp)
        raise typer.Exit()

    ctx.obj = mp
