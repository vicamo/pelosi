"""Pelosi command line interface."""
# pelosi/cli.py

import typer
from typing import Optional

from pelosi import __app_name__, __version__
from pelosi.entities import (
    branchmergeproposal,
    gitrepositories,
    gitrepository,
)
from pelosi.launchpad import get_launchpad

app = typer.Typer()
app.add_typer(branchmergeproposal.app, name="branch-merge-proposal")
app.add_typer(gitrepositories.app, name="git-repositories")
app.add_typer(gitrepository.app, name="git-repository")
option_anonymous = False
option_instance = None


def _version_callback(value: bool) -> None:
    if value:
        typer.echo(f"{__app_name__} v{__version__}")
        raise typer.Exit()


def _anonymous_callback(value: bool) -> None:
    global option_anonymous
    option_anonymous = value


def _instance_callback(value: str) -> None:
    global option_instance
    option_instance = value


@app.callback(invoke_without_command=True)
def main(
    ctx: typer.Context,
    version: Optional[bool] = typer.Option(
        None,
        "--version",
        "-v",
        help="Show the application's version and exit.",
        callback=_version_callback,
        is_eager=True,
    ),
    anonymous: Optional[bool] = typer.Option(
        None,
        "--anonymous",
        "-A",
        help="Login anonymously.",
        callback=_anonymous_callback,
    ),
    instance: Optional[str] = typer.Option(
        None,
        "--instance",
        "-i",
        help="Service instance.",
        callback=_instance_callback,
    ),
) -> None:
    if ctx.invoked_subcommand:
        ctx.obj = get_launchpad(option_instance, option_anonymous)

    return
