"""Pelosi launchpadlib utilities."""
# pelosi/cli.py

from datetime import date, datetime
from enum import Enum
import json
import os

from launchpadlib.launchpad import Launchpad

from pelosi import __app_name__


__all__ = [
    "BranchMergeProposalQueueStatus",
    "GitRepositoriesSortOrder",
    "GitRepositoryInformationType",
    "get_launchpad",
    "pretty_dumps",
]


class BranchMergeProposalQueueStatus(str, Enum):
    WORK_IN_PROGRESS = "Work in progress"
    NEEDS_REVIEW = "Needs review"
    APPROVED = "Approved"
    REJECTED = "Rejected"
    MERGED = "Merged"
    CODE_FAILED_TO_MERGE = "Code failed to merge"
    QUEUED = "Queued"
    SUPERSEDED = "Superseded"


class GitRepositoriesSortOrder(str, Enum):
    interesting = "by most interesting"
    name = "by repository name"
    changed = "most recently changed first"
    neglected = "most neglected first"
    newest = "newest first"
    oldest = "oldest first"


class GitRepositoryInformationType(str, Enum):
    PUBLIC = "Public"
    PUBLIC_SECURITY = "Public Security"
    PRIVATE = "Private"
    PRIVATE_SECURITY = "Private Security"
    PROPRIETARY = "Proprietary"
    EMBARGOED = "Embargoed"


def get_launchpad(instance, anonymous):
    if instance is None:
        instance = os.getenv("LPINSTANCE")
    if instance is None:
        instance = "production"

    if anonymous:
        launchpad = Launchpad.login_anonymously(__app_name__, instance, version="devel")
    else:
        launchpad = Launchpad.login_with(
            __app_name__,
            instance,
            version="devel",
            consumer_name=__app_name__,
        )

    return launchpad


def _json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, (datetime, date)):
        return obj.isoformat()
    raise TypeError("Type %s not serializable" % type(obj))


def pretty_dumps(res, pretty):
    f = None
    if pretty.startswith("format:"):
        f = pretty[7:]
        pretty = "format"

    if pretty == "self-link":
        return res.self_link

    d = dict((attr, res.__getattr__(attr)) for attr in res.lp_attributes)
    if pretty == "format":
        return f.format(**d)

    return json.dumps(d, default=_json_serial, indent=2)
